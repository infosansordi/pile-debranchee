# Convertir tous les SVG en PDF
SOURCE_FIGS := $(wildcard fig/*.svg)

.PHONY : all 
all : $(SOURCE_FIGS:.svg=.pdf)

# svg 2 pdf
%.pdf: %.svg
	inkscape -A $@ $<

